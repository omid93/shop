<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class CategoryController{

	public function index($request) {
		$data = [];
		View::load('panel.category.index',$data,'panel-admin');
	}
	
	public function attributes($request) {
		$data = [];
        View::load('panel.category.attributes', $data, 'panel-admin');
    }

}