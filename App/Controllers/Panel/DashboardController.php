<?php
namespace App\Controllers\Panel;

use App\Models\Option;
use App\Repositories\OptionRepo;
use App\Services\View\View;

class DashboardController{

    public function index($request) {
        $optionRepo = new OptionRepo();

        $data = [];
/*        $allOptions = new Option();
        foreach ($allOptions::all() as $option){
            var_dump($option);
        }

        foreach ($optionRepo->uiOptions() as $option){
            var_dump($option);
        }
        for ($i=0 ; $i<3 ; $i++){
            $rand = rand(1000,10000);
            $option = [
                'option_slug' => "optSS_$rand",
                'option_value' => "optVal_$rand",
                'option_title' => "optTitle_$rand",
                'option_cat' => "parameter"
            ];
//           $op = new Option($option);
//           $op->save();

            $optionRepo->create($option);
        }
        */

        View::load('panel.dashboard.index',$data,'panel-admin');
    }

}