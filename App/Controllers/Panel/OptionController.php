<?php
namespace App\Controllers\Panel;

use App\Core\Request;
use App\Models\Option;
use App\Repositories\OptionRepo;
use App\Services\View\View;

class OptionController{

    public function index($request)
    {
        $optionRepo = new OptionRepo();
        $data = [
            'options' => $optionRepo->all()
        ];

        View::load('panel.options.index', $data, 'panel-admin');
    }

    public function add($request)
    {
        View::load('panel.options.add', [], 'panel-admin');
    }

    public function create(Request $request)
    {
        $optionRepo = new OptionRepo();
        $optionRepo->create($request->params());

//        View::load('panel.options.add', [], 'panel-admin');
    }

}