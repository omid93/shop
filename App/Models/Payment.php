<?php
namespace App\Models;

class Payment extends BaseModel {
	protected $table = 'payments' ;
	protected $primaryKey = 'id' ;

    public function order(){
        return $this->belongsTo(Order::class,'order_id');
    }

}