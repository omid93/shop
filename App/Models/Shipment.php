<?php
namespace App\Models;

class Shipment extends BaseModel {
	protected $table = 'shipment' ;
	protected $primaryKey = 'id' ;

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }
}