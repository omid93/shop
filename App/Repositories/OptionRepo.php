<?php
namespace App\Repositories;
use App\Models\Option;

class OptionRepo extends BaseRepo {
    protected $model = Option::class;

    public function uiOptions()
    {
        return $this->model::where('option_cat','ui')->get();
    }


}