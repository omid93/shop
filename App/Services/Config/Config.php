<?php
namespace App\Services\Config;

class Config{
	public static function get($config) {
		return include CONFIG.$config.".php";
	}

	public static function getConfigItem($config,$item) {
		$config =  include CONFIG.$config.".php";
		if(array_key_exists($item,$config)){
			return $config[$item];
		}
	}


}