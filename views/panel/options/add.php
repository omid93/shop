<div class="page-wrapper ssWrap ss-dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card p-30">
                    <form action="<?php echo siteUrl('panel/options/create'); ?>" method="post">
                        <div class="form-group">
                            <label>نامک تنظیمات</label>
                            <input type="text" name="option_slug" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>عنوان فارسی تنظیمات</label>
                            <input type="text" name="option_title" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>مقدار</label>
                            <textarea class="form-control" name="option_value" rows="3" placeholder="مقدار تظیمات رو وارد کنید"></textarea>
                        </div>
                        <div class="form-group">
                            <label>انتخاب یک گزینه</label>
                            <select name="option_cat" class="form-control">
                                <option value="ui">تنظیمات ظاهری</option>
                                <option value="parameter">پارامتر های فروشگاه</option>
                                <option value="general">تنظیمات عمومی</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info btn-flat btn-addon m-b-10">افزدن</button>
                    </form>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card p-30">
                    Update Options
                </div>
            </div>
        </div>
    </div>
</div>
